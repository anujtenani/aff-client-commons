const translate = require('@vitalets/google-translate-api');
const en = require('./en');
const langs = ['fr'];

const p = langs.map((lang) => {
	const promises = Object.keys(en).map((item) => {
		return translate(en[item], { to: lang }).then(res => ({
			key: item,
			val: res.text
		})).catch(err => {
			console.log(err);
			return {
				key: item,
				val: undefined
			};
		});
	});

	return Promise.all(promises).then((res) => {
		const langMap = {};
		res.forEach((item) => {
			langMap[item.key] = item.val;
		});
		const str = `module.exports = ${JSON.stringify(langMap, null, 2)}`;
		require('fs').writeFileSync(require('path').join(__dirname,`${lang}.js`), str);
	});
});


Promise.all(p).then(() => process.exit(200));
