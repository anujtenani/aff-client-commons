module.exports = {

	/**
	 * Generic
	 */
	tryAgain: 'Try Again',
	contactUs: 'Contact us',
	navlinkHome: 'Home',
	navlinkProducts: 'Products',
	navlinkPayments: 'Payments',
	paymentsSubtitle:" ",
	navlinkSettings: 'Settings',
	logout: 'Logout',
	accountBannedMessage: 'Your account is blocked! You will no longer receive credit for your referrals.',

	/**
	 * Home Page
	 */
	referralLink: 'Referral Link',
	referralLinkBadge:'{commission_value}',
	referralLinkDesc: 'Refer your friends using the link below and earn commissions on purchases made by them',

	/**
 	* Settings
	*/
	referralCode: 'Referral Code',
	referralCodeDesc: 'Customize your URL referral code',
	Profile: 'Profile',
	ProfileDesc: 'Update your profile',
	PaymentSettings: 'Payment Settings',
	PaymentSubtitle: 'How would you like to get paid ?',
	paymentTerm: 'Payment Term',
	minimumPayout: 'Minimum Payout',

	/**
	 * payments strings
	 */
	Download: 'Download',
	amountPaid: 'Amount Paid',
	amountPending: 'Amount Pending',
	lastPayment: 'Last Payment',
	summary: 'Summary',
	details: 'Details',
	noPayments: 'No payments have been made yet',

	/**
	 * Products Page
	 */
	productLinks: 'Product Links',
	productLinkSubtitle: 'Browse the store for qualifying products. Paste the URL below to generate an affiliate link',
	productLinkDescription: 'Share this link on your blog, Facebook, Instagram, etc. When people visit the store using your link, you get commissions on everything they buy',

	productPageLinkText: 'Paste product page link',
	generatedAffiliateLinkText: 'Your generated link',
	createLinkButton: 'Create Link',

	/**
	 * coupons ref
	 */
	couponCode: 'Coupon Code',
	couponCodeSubtitle: 'Share your coupon code with others. For every purchase someone makes using your coupon code, you get the credit',
	changeCoupon: 'Change coupon code',
	noCouponGiven: 'No coupon code found! You can request us for a coupon code to share with your followers. {contact_us}',

	/**
	 * Media Assets
	 */
	mediaAssets: 'Media Assets',
	Cancel: 'Cancel',
	Submit: 'Submit',
	Update: 'Update',
	Change: 'Change',
	Setup: 'Setup',
	SaveChanges: 'Save Changes',
	changeCouponModalDescription: 'Change coupon code as per your preference. Your old code will be invalidated',

	/**
	 * Authentication
	 */
	createAccount: 'Create Account',
	"Create Account":"Create Account",
	emailAddress: 'Email Address',
	password: 'Password',
	yourName: 'Your Name',
	"Your Name": 'Your Name',
	Login: 'Login',
	alreadyHaveAccount: 'Already have an account ?',
	dontHaveAccount: 'Don\'t have an account ?',
	tosAgree: 'By creating an account you are explicitly agreeing to our Terms of Service & Privacy Policy',
	signInGoogle: 'Google',
	signInFacebook: 'Facebook',
	forgotPassword: 'Forgot Password',

	emailVerified: 'Email Address Verified',
	emailVerifiedThanks: 'Thank you for confirming your email address.',
	returnToDashboard: 'Return to Dashboard',
	verifyingEmail: 'Verifying email address',

	/**
	 * Password recovery
	 */
	recoverPassword: 'Recover Password',
	recoverPasswordFormDesc: 'Enter your email address below. We will send you a link to reset your password on your email',
	recoverLinkSent: 'A password reset link has been sent to your email address {email}',
	resetPassword: 'Reset Password',
	newPassword: 'New Password',

	/**
	 * Landing Page
	 */
	pageTitle: 'Affiliate Portal',
	joinNow: 'Join Now',
	heroline: 'Profit from our experience',
	herosubtext:'',
	heroDescHasCommission: 'Earn up to {commission_value} commission for every successful referral',
	heroDescNoCommission: 'Earn commissions on successful referrals',
	sectionHeadingHowItWorks: 'How does it work ?',
	sectionCardFirstTitle: 'Join',
	sectionCardFirstDetail: 'It\'s free and easy to join.\n\nGet up and running today.\n\n{join_now}',
	sectionCardSecondTitle: 'Advertise',
	sectionCardSecondDetail: 'Choose from our products to advertise to your customers.\n\nWhether you are a large network, content site, social media influencer or blogger, we have simple linking tools to meet your advertising needs and help you monetize\n\n{link_tools}',
	linkingTools: 'Linking Tools',
	productCommissions: 'Product Commissions',
	"Commission Details":"Commission Details",
	reportingTools: 'Reporting Tools',
	Reports:"Reports",
	Tools:"Tools",
	sectionCardThirdTitle: 'Earn',
	sectionCardThirdCommDetail: 'Get up to {commission_value} in commissions on successful referrals. \n\nEarn commissions from all qualifying purchases, not just the products you advertised. Plus, our competitive conversion rates help you maximize your earnings.\n\n{link_commissions}',

	footerLearnMore: 'Learn',
	footerCustomerSupport: 'Customer Support',
	footerLegal: 'Legal',
	footerCommissions: 'Commissions',
	footerTools: 'Tools',
	footerReporting: 'Reporting',
	footerTerms: 'Terms & Conditions',
	privacyPolicy: 'Privacy Policy',
	footerFeedbackTitle: 'What do you think?',
	footerFeedbackDescription: 'Do you have a suggestion or comment about our program?',
	footerFeedbackLetUsKnow: 'Let us know.',

	/**
	 * Contact Page
	 */
	pageDesc: 'Send us your queries, suggestions, issues etc. using the form below. We will get back to you as soon as possible',
	issueTypeLabel: 'Select Issue Type',
	issueTypeGeneral: 'General Issues/Queries',
	issueTypeTechnical: 'Technical Issues/Queries',
	issueTypeFeedback: 'Feedback/Suggestions',
	issueTypePayment: 'Payment Issues/Queries',
	issueTypeDelAcct: 'Request Account Deletion',
	yourMessage: 'Your message',
	sendMessage: 'Send Message',
	messageSentSuccess: 'Thank you for contacting us. We have received your message and will get back to you shortly',

	/**
	 * Email Verification
	 */
	emailUnverified: 'We have sent you an account verification email. Kindly check your inbox. Didn\'t get the mail ?',
	resendEmail: 'Resend Verification Email',
	emailSent: 'Email Sent',
	accountPendingVerification: 'Your account is still under verification. Please allow up to 24-48 for the account to get verified. Kindly {updateYourProfile} in the meantime',
	updateYourProfile: 'update your profile',

	/**
	 * Sales Section
	 */
	Referrals: 'Referrals',
	Orders: 'Orders',
	Conversions: 'Conversions',
	Sales: 'Sales',
	Earnings: 'Earnings',
	'Team sales':'Team sales',
	thisMonth: 'This month',
	lastMonth: 'Last month',
	last30Days: 'Last 30 days',
	allTime: 'All time',

	/**
	 * Error Messages
	 */
	REGISTRATIONNOTALLOWED: 'Sorry! We are currently not accepting new registrations',
	EMAILREQUIRED: 'Email address is required',
	USEREXISTS: 'You already have an account with us.',
	clickHereToLogin: 'Click here to login',
	EMAILINVALID: 'Please check your email address. It seems incorrect',
	INCORRECTPASSWORD: 'Email address or password is incorrect.',
	clickHereToResetPwd: 'Click here to reset your password',
	createAnAccount: 'Create an account',
	LINKEXPIRED: 'Password reset link has expired. To generate a new link, use the Forgot Password option.',
	NETWORKERROR: 'Network error.',
	USERDOESNOTEXISTS: 'You do not have an account with us.',
	"Create Password":"Create Password",
	"Show password":"Show password",

	/**
	 * MLM
	 */
	Network: 'Network',
	'Network Summary': 'Network Summary',
	'Network Commissions': 'Network Commissions',
	'Partner signup referral link': 'Partner signup referral link',
	'Get commissions when your sub affiliates refer customers': 'Get commissions when your sub affiliates refer customers',
	Commissions: 'Commissions',
	'My Network': 'My Network',
	'No direct referrals yet': 'No direct referrals yet',
	Commission: 'Commission',
	Level: 'Level',
	'Direct Referrals': 'Direct Referrals',
	'Indirect Referrals': 'Indirect Referrals',

	/**
	 * Bank Input
	 */
	'Bank Transfer': 'Bank Transfer',
	'Store Discount Coupon': 'Store Discount Coupon',
	'Bank Name': 'Bank Name',
	'Account Type': 'Account Type',
	Checking: 'Checking',
	Saving: 'Saving',
	'Account Name': 'Account Name',
	'Account Number': 'Account Number',
	'Branch Code': 'Branch Code',
	'Swift Code': 'Swift Code',

	/**
	 * Create account options
	 */
	Phone: 'Phone',
	Comment: 'Comment',
	Country: 'Country',
	'Hide Links': 'Hide Links',
	'Show Links': 'Show Links',
	'Invite others, grow your network, and earn commissions': 'Invite others, grow your network, and earn commissions',
	'Signup Link': 'Signup Link',
	'Home Page': 'Home Page',
	Details: 'Details',
	Members: 'Members',
	Revenue: 'Revenue',
	Commisssion: 'Commission',

	'Copy Shareable Link': 'Copy Shareable Link',
	Copy: 'Copy',
	Copied: 'Copied',
	'Net 7': 'Net 7',
	'Net 15': 'Net 15',
	'Net 30': 'Net 30',
	OR: 'OR',
	'Go Back': 'Go Back',
	'Debit Card': 'Debit Card',
	'Debit Card Number': 'Debit Card Number',
	'First Name': 'First Name',
	'Last Name': 'Last Name',
	Name: 'Name',
	'Document Number': 'Document Number',
	required: '* - Required',
	Password: 'Password',
	'Marketing Tools': 'Marketing Tools',
	coupon_code_discount: '{discount_value} off',
	'Payment Mode': 'Payment Mode',
	'No payment mode set': 'No payment mode set',
	Primary: 'Primary',
	'Referral codes (one per line)': 'Referral codes (one per line)',
	'First one will be set as primary': 'First one will be set as primary',
	'Change Password': 'Change Password',
	Date: 'Date',
	Order: 'Order',
	Amount: 'Amount',
	'Top Selling Products': 'Top Selling Products',
	"Network Map":"Network Map",
	"Address":"Address",
	"Zip Code":"Zip Code",
	"City":"City",
	"State":"State",
	"Postal Code":"Postal Code",
	"Street Address":"Street Address",
	"Tax Identification Number":"Tax Identification Number",
	"EIN or SSN or PAN (whichever is applicable)" : "EIN or SSN or PAN (whichever is applicable)",
	"Your mailing address":"Your mailing address",
	"I agree to terms and conditions":"I agree to the terms and conditions",
	"Your rank":"Your rank",
	"You":"You",
	"Analytics":"Analytics",
	"Loading Data...It might take a while":"Loading Data...It might take a while",
	"Browsers":"Browsers",
	"Operating Systems":"Operating Systems",
	"Devices":"Devices",
	"Click Origins":"Click Origins",
	"Landing Pages":"Landing Pages",
	"Visits":"Visits",
	"Traffic Analysis":"Traffic Analysis",
	"This Week":'This Week',
	"We are not accepting registrations at this moment. Kindly check back later":"We are not accepting registrations at this moment. Kindly check back later",
	"Cheque":"Cheque",
	"Commission Structure":"Commission Structure",
	"Product":"Product",
	"Category":"Category",
	"A {commission_value} commission is given on sale of every product (except those listed below)":"A {commission_value} commission is given on sale of every product (except those listed below)",
	"Pages":"Pages",
	"You have not published any pages yet. {create_page_link}":"You have not published any pages yet. {create_page_link}",
	"Create a new page":"Create a new page",
	"New Page":"New Page",
	"Edit Page":"Edit Page",
	"Save Page":"Save Page",
	"Preview Page":"Preview Page",
	"Under review":"Under review",
	"Published":"Published",
	"Page Title":"Page Title",
	"Copy Link":"Copy Link",
	"Copy Text":"Copy Text",
	"Copy as HTML":"Copy as HTML",
	"Download Media":"Download Media",
	"Account under review":"Account under review",
	"My Files":"My Files",
	"Upload your files":"Upload your files",
	"Add File" :"Add File",
	"File description":"File description",
	"File name":"File name",


	"Level 1":"Level 1",
	"Level 2":"Level 2",
	"Level 3":"Level 3",
	"Level 4":"Level 4",
	"Level 5":"Level 5",
	"Level 6":"Level 6",
	"Level 7":"Level 7",
	"Level 8":"Level 8",
	"Level 9":"Level 9",
	"Level 10":"Level 10",
	"Domain":"Domain",
	"Conversion":"Conversion",
	"Origin":"Origin",
	"Origin Domain":"Origin Domain",
	"Source":"Source",
	"Landing Page":"Landing Page",
	"Custom Query Param":"Custom Query Param",
	"Query Parameter":"Query Parameter",
	"Aggregate By":"Aggregate By",
	"No Data":"No Data",
	"Quantity":"Quantity",
	"Price":"Price",
	"Customer":"Customer",
	"Order Total":"Order Total",
	"Products Sold":"Products Sold",
	"networkLink":"Network Link",
	"Discount Adjustment":"Discount Adjustment",

	// My Pages
	"My Pages":"My Pages",
	"Are you sure you want to delete this page ?":"Are you sure you want to delete this page ?",
	"Sponsor":"Sponsor",
	"Referred by":"Referred by",

	"Gift Card":"Gift Card",
	"Customers":"Customers",
	"Show all":"Show all",

	"sale_approved":"approved",
	"sale_pending":"pending",

	"Traffic":"Traffic",
	"Direct":"Direct",
	"Sort By":"Sort By",
	"Shipping Address":"Shipping Address",

	//Discount page
	"discount_page_heading":"Here is your {discount} OFF",
	"discount_page_text":"{affiliate_name} thinks you'll love {store_name}, so here's {discount} off!",
	"SHOP NOW":"SHOP NOW",

	"leaderboard_rank":"{amount} behind #{rank}",


	//autopay
	"Use discount code":"Use discount code",
	"Minimum Amount":"Minimum: {amount}",
	"Maximum Amount":"Maximum: {amount}",
	"Spend":"Spend",
	"Move the slider to select the amount you want to redeem":"Move the slider to select the amount you want to redeem",
	"Get discount code worth":"Get discount code worth",
	"Redeem":"Redeem",
	"Here is your amount discount coupon":"Here is your {amount} discount coupon",
	'Due in x days': 'Due in {x} days',

	// affiliate membership

	"recurring_plan_daily":'{num, plural, one {day} other {{num} days}}',
	"recurring_plan_weekly":'{num, plural, one {week} other {{num} weeks}}',
	"recurring_plan_monthly":'{num, plural, one {mo} other {{num} mo}}',
	"recurring_plan_yearly":'year',
	"Activate membership":"Activate membership",
	"Continue":"Continue",
	"Choose":"Choose",

	"Current Password":"Current password",
	"Confirm Password":"Confirm password",
	"Password was successfully changed":"Password was successfully changed",
	"Passwords do not match":"Passwords do not match",

	"Profile Updated":"Profile Updated",


	"Personal Discount":"Personal Discount",
	"Personal discount description":"Use the following coupon code for your personal purchases to receive discounts on your order",

	"Free shipping":"Free shipping",
	"This month":"This month",
	"Last month":"Last month",
	"Last 30 days":"Last 30 days",
	"All time":"All time",

	"OK":"OK",
	"Bank Transfer (SEPA)":"Bank Transfer (SEPA)",
	"PayPal email address":"PayPal email address",
	"Website":"Website",

	"Add to cart":"Add to cart",
	"Store": "Shop",
	"Shop":"Shop",
	"Download invoice":"Download invoice",
	"Transactions":'Transactions',

	// notification preferences
	"Notification preferences":"Notification preferences",
	"New sale notification":"New sale notification",
	"Sale updated notification":"Sale updated notification",
	"Payment processed notification":"Payment processed notification",
	"Newsletter email":"Newsletter email",
	//affiliate store
	"Available: Inventory":"Available: {totalInventory, number}",
	"Complete Checkout":"Complete Checkout",
	"Remove":"Remove",
	"Open Checkout page":"Open Checkout page",
	"Checkout":"Checkout",

	//network explorer
	"Network explorer":"Network explorer",
	"Number of sales":"Number of sales",
	"Total sale volume":"Total sale volume",
	"Total earnings":"Total earnings",
	"Summary":"Summary",

	"Status":"Status",

	"Add note":"Add note",
	"Note":"Note",

	"Search":"Search",
	"Show active affiliates":"Show active affiliates",

	//transactions page
	"Transaction":"Transaction",
	"Final Balance":"Final Balance",
	"Pending":"Pending",
	"Approved":"Approved",
	"Deleted":"Deleted",
	"Rejected":"Rejected",
	"Network commission":"Network commission",
	"Wallet Adjustment":"Wallet adjustment",
	"Signup Bonus":"Signup Bonus",
	"Recruitment Bonus":"Recruitment Bonus",
	"Target Bonus":"Target Bonus",
	"Page currentPageNumber of totalPages":"Page {page, number} of {totalPages, number}",

	"networkCommissionDescription":""
};
