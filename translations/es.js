module.exports = {
  "tryAgain": "Inténtalo nuevamente",
  "contactUs": "Contactanos",
  "navlinkHome": "Principal",
  "navlinkProducts": "Generador de Links",
  "navlinkPayments": "Resumen de cuenta",
  "navlinkSettings": "Ajustes",
  "logout": "Cerrar sesión",
  "accountBannedMessage": "Su cuenta ha sido bloqueada. Sus referencias ya no serán acreditadas. Para apelar esta prohibición, contáctenos en partners@goaffpro.com",
  "referralLink": "Tu enlace de referencia",
  "referralLinkBadge": "{commission_value}",
  "referralLinkDesc": "Envía el siguiente enlace a tus contactos para comisionar con sus compras ",
  "referralCode": "código de referencia",
  "referralCodeDesc": "Modifica el código de referencia para tus conocidos",
  "Profile": "Datos personales",
  "ProfileDesc": "Actualiza tus datos personales para obtener mejores resultados",
  "PaymentSettings": "Configuración medios de cobro",
  "PaymentSubtitle": "¿Cómo te gustaría recibir tus comisiones?",
  "paymentTerm": "Plazo de pago",
  "minimumPayout": "Pago mínimo",
  "Download": "Descargar",
  "amountPaid": "Conbros totales",
  "amountPending": "Cantidad pendiente",
  "lastPayment": "Ultimo cobro",
  "summary": "Resumen",
  "details": "Detalles",
  "noPayments": "Aún no se han efectuado pagos.",
  "productLinks": "Enlaces de productos",
  "productLinkSubtitle": "Busca en la tienda su producto favorito. Pega la URL a continuación para generar un enlace de afiliado",
  "productLinkDescription": "Comparte este enlace de afiliado en tu Blog, Facebook, Instagram, etc. Cuando las personas visitan la tienda utilizando su enlace, obtiene comisiones por todo lo que compran.",
  "productPageLinkText": "Pegar enlace del producto",
  "generatedAffiliateLinkText": "Su enlace de afiliado es:",
  "createLinkButton": "Crear vínculo",
  "couponCode": "Código promocional",
  "couponCodeSubtitle": "Comparte tu cupón de descuento con tus conocidos. Por cada compra que hagan usando el código de cupón, obtendrás el crédito",
  "changeCoupon": "Cambiar código de cupón",
  "noCouponGiven": "No se te ha dado un cupón para compartir. Puede solicitarnos un cupón para compartir con sus seguidores.",
  "mediaAssets": "Medios de comunicación",
  "Cancel": "Cancelar",
  "Submit": "Enviar",
  "Update": "Actualizar",
  "Change": "Modificar",
  "Setup": "Preparar",
  "SaveChanges": "Guardar cambios",
  "changeCouponModalDescription": "Cambiar el código de cupón según su preferencia. Tu antiguo código será invalidado",
  "createAccount": "Crear una cuenta",
  "Create Account": "Crear una cuenta",
  "emailAddress": "Dirección de correo electrónico",
  "password": "Contraseña",
  "yourName": "Tu nombre",
  "Your Name": "Tu nombre",
  "Login": "Iniciar sesión",
  "alreadyHaveAccount": "Ya tienes una cuenta ?",
  "dontHaveAccount": "¿No tienes una cuenta?",
  "tosAgree": "Al crear una cuenta, usted acepta explícitamente nuestros Términos de servicio y Política de privacidad",
  "signInGoogle": "Inicia sesión con Google",
  "signInFacebook": "Iniciar sesión usando Facebook",
  "forgotPassword": "Se te olvidó tu contraseña",
  "emailVerified": "Correo electrónico verificado",
  "emailVerifiedThanks": "Gracias por confirmar tu dirección de correo electrónico.",
  "returnToDashboard": "Regresar al Panel principal",
  "verifyingEmail": "Verificando la dirección de correo electrónico",
  "recoverPassword": "Recuperar contraseña",
  "recoverPasswordFormDesc": "Introduzca su dirección de correo electrónico a continuación. Te enviaremos un enlace para restablecer su contraseña en su correo electrónico.",
  "recoverLinkSent": "Se ha enviado un enlace de restablecimiento de contraseña a Tu dirección de correo electrónico {email}",
  "resetPassword": "Restablecer contraseña",
  "newPassword": "Nueva contraseña",
  "pageTitle": "Pagina de Afiliados",
  "joinNow": "Únirte ahora",
  "heroline": "Aprovecha nuestra experiencia.",
  "herosubtext": "",
  "heroDescHasCommission": "Gana hasta la comisión de {commission_value} por cada cliente que generes",
  "heroDescNoCommission": "Gana comisiones por por ventas",
  "sectionHeadingHowItWorks": "¿Como funciona ?",
  "sectionCardFirstTitle": "Unirse",
  "sectionCardFirstDetail": "Es gratis y fácil de unirse. Ingresa hoy y empeza a ganar comisiones.\n\n{join_now}",
  "sectionCardSecondTitle": "Anunciar",
  "sectionCardSecondDetail": "Si tenes muchos contactos en tus redes, un sitio de contenido o sos influyente de redes sociales , tenemos contenidos diseñados precisamente para que los puedas compartirlos y dar a conocer nuestros productos. Te aseguramos que lograras ventas exitosas rápidamentes.\n\n{link_tools}",
  "linkingTools": "Herramientas de enlace",
  "productCommissions": "Comisiones de productos",
  "Commission Details": "Detalles de la comisión",
  "reportingTools": "Herramientas de informes",
  "Reports": "Informes",
  "Tools": "Herramientas",
  "sectionCardThirdTitle": "Ganar",
  "sectionCardThirdCommDetail": "Obtenga hasta {commission_value} en comisiones por venta. Gana comisiones de todas las compras que generes, no solo de los productos que anunció. Compratiendo el link, la persona que ingrese podrá ver otros productos y si realiza una compra sea cual será el articulo comisionaras de todos modos. Además, podemos asegurar que nuestro circuito de venta esta optimizado para que el cliente tenga la mejor experiencia de compra. \n\n{link_commissions}",
  "footerLearnMore": "Aprender",
  "footerCustomerSupport": "Atención al cliente",
  "footerLegal": "Legal",
  "footerCommissions": "Comisiones",
  "footerTools": "Herramientas",
  "footerReporting": "Informes",
  "footerTerms": "Términos y condiciones",
  "privacyPolicy": "Política de privacidad",
  "footerFeedbackTitle": "¿Qué piensas?",
  "footerFeedbackDescription": "¿Tiene alguna sugerencia o comentario sobre nuestro programa de afiliados?",
  "footerFeedbackLetUsKnow": "Escribinos saber.",
  "pageDesc": "Envíenos sus consultas, sugerencias, problemas, etc. utilizando el formulario a continuación. Nos pondremos en contacto con vos lo antes posible.",
  "issueTypeLabel": "Seleccione el tipo de consulta",
  "issueTypeGeneral": "consultas generales / consultas",
  "issueTypeTechnical": "Problemas técnicos / consultas",
  "issueTypeFeedback": "Comentarios / Sugerencias",
  "issueTypePayment": "Consutlas sobre pagos / consultas",
  "issueTypeDelAcct": "Solicitud de eliminación de cuenta",
  "yourMessage": "Tu mensaje",
  "sendMessage": "Enviar mensaje",
  "messageSentSuccess": "Gracias por contactarnos. Hemos recibido tu mensaje y te responderemos en breve.",
  "emailUnverified": "Te hemos enviado un correo electrónico de verificación de cuenta. Por favor revise tu bandeja de entrada. ¿No recibió el correo?",
  "resendEmail": "Reenviar correo electrónico de verificación",
  "emailSent": "Email enviado",
  "accountPendingVerification": "Su cuenta todavía está bajo verificación. Por favor permita hasta 24-48 para que la cuenta sea verificada. Amablemente {updateYourProfile} mientras tanto",
  "updateYourProfile": "Actualiza tu perfil",
  "Referrals": "Referidos",
  "Orders": "Pedidos",
  "Conversions": "Conversiones",
  "Sales": "Ventas",
  "Earnings": "Ganancias",
  "Team sales": "Ventas de equipo",
  "thisMonth": "Este mes",
  "lastMonth": "El mes pasado",
  "last30Days": "Últimos 30 días",
  "allTime": "Todo el tiempo",
  "REGISTRATIONNOTALLOWED": "¡Lo siento! Actualmente no estamos aceptando nuevas inscripciones.",
  "EMAILREQUIRED": "Se requiere E-mail",
  "USEREXISTS": "Ya tienes una cuenta con nosotros.",
  "clickHereToLogin": "Haga clic aquí para ingresar",
  "EMAILINVALID": "Por favor, comprueba tu dirección de correo electrónico. Parece incorrecto",
  "INCORRECTPASSWORD": "La dirección de correo electrónico o contraseña es incorrecta.",
  "clickHereToResetPwd": "Pincha aquí para restaurar tu contraseña",
  "createAnAccount": "Crea una cuenta",
  "LINKEXPIRED": "El enlace para restablecer la contraseña ha caducado. Para generar un nuevo enlace, use la opción Olvidé mi contraseña.",
  "NETWORKERROR": "Error de red.",
  "USERDOESNOTEXISTS": "No tienes cuenta con nosotros.",
  "Create Password": "Crear contraseña",
  "Show password": "Mostrar contraseña",
  "Network": "Tus Afiliados",
  "Network Summary": "Reporte de tus Afiliados",
  "Network Commissions": "Comisiones de tus Socios",
  "Partner signup referral link": "Enlace para tus Socios",
  "Get commissions when your sub affiliates refer customers": "Obtene comisiones cuando tus socios recomienden y generen ventas en nuestro sitio ",
  "Commissions": "Comisiones",
  "My Network": "Resumen de Mis Socios",
  "No direct referrals yet": "No hay información todavía",
  "Commission": "Comisión",
  "Level": "Nivel",
  "Direct Referrals": "Miembros Directos",
  "Indirect Referrals": "Miembros Indirectos",
  "Bank Transfer": "Transferencia bancaria",
  "Store Discount Coupon": "Cupón de descuento de la tienda",
  "Bank Name": "Nombre del banco",
  "Account Type": "Tipo de cuenta",
  "Checking": "Cuenta Corriente",
  "Saving": "Caja de Ahorro",
  "Account Name": "Nombre del Banco",
  "Account Number": "Número de cuenta",
  "Branch Code": "Numero de sucursal",
  "Swift Code": "CBU",
  "Phone": "Teléfono",
  "Comment": "Comentario",
  "Country": "País",
  "Hide Links": "Ocultar enlaces",
  "Show Links": "Mostrar enlaces",
  "Invite others, grow your network, and earn commissions": "Invita a otros, haz crecer tu red y gana comisiones",
  "Signup Link": "Enlace de registro",
  "Home Page": "Página de inicio",
  "Details": "Detalles",
  "Members": "Miembros",
  "Revenue": "Ingresos",
  "Commisssion": "Comisión",
  "Copy Shareable Link": "Copiar Enlace Compartible",
  "Copy": "Copiar",
  "Copied": "Copiado",
  "Net 7": "Red 7",
  "Net 15": "Neto 15",
  "Net 30": "Neto 30",
  "OR": "O",
  "Go Back": "Regresa",
  "Debit Card": "Tarjeta de débito",
  "Debit Card Number": "Numero de tarjeta de débito",
  "First Name": "Primer Nombre",
  "Last Name": "Apellido",
  "Name": "Nombre",
  "Document Number": "Número de Documento",
  "required": "* - Necesario",
  "Password": "Contraseña",
  "Marketing Tools": "Herramientas de marketing",
  "coupon_code_discount": "{discount_value} de descuento",
  "Payment Mode": "Modo de pago",
  "No payment mode set": "No hay modo de pago establecido",
  "Primary": "Primario",
  "Referral codes (one per line)": "Códigos de referencia (uno por línea)",
  "First one will be set as primary": "El primero se establecerá como primario",
  "Change Password": "Cambia la contraseña",
  "Date": "Fecha",
  "Order": "Pedido",
  "Amount": "Cantidad",
  "Top Selling Products": "Los productos más vendidos",
  "Network Map": "Mapa de red",
  "Address": "Dirección",
  "Zip Code": "Código postal",
  "City": "Ciudad",
  "State": "Estado",
  "Postal Code": "Código postal",
  "Street Address": "Dirección",
  "Tax Identification Number": "Número de identificación de impuestos",
  "EIN or SSN or PAN (whichever is applicable)": "EIN o SSN o PAN (lo que sea aplicable)",
  "Your mailing address": "Su dirección de correo",
  "I agree to terms and conditions": "Estoy de acuerdo con los términos y condiciones",
  "Your rank": "Su rango",
  "You": "usted",
  "Analytics": "Analítica",
  "Loading Data...It might take a while": "Cargando datos ... Puede llevar un tiempo",
  "Browsers": "Navegadores",
  "Operating Systems": "Sistemas operativos",
  "Devices": "Dispositivos",
  "Click Origins": "Click Origins",
  "Landing Pages": "Páginas de destino",
  "Visits": "Visitas",
  "Traffic Analysis": "Análisis de tráfico",
  "This Week": "Esta semana",
  "We are not accepting registrations at this moment. Kindly check back later": "No estamos aceptando registros en este momento. Por favor, vuelva más tarde",
  "Cheque": "Cheque",
  "Commission Structure": "Estructura de la comisión",
  "Product": "Producto",
  "Category": "Categoría",
  "A {commission_value} commission is given on sale of every product (except those listed below)": "Se otorga una comisión {commission_value} en la venta de cada producto (excepto los que se enumeran a continuación)",
  "Pages": "Páginas",
  "You have not published any pages yet. {create_page_link}": "Aún no has publicado ninguna página. {create_page_link}",
  "Create a new page": "Crea una nueva página",
  "New Page": "Nueva pagina",
  "Edit Page": "Editar página",
  "Save Page": "Guardar página",
  "Preview Page": "Página de vista previa",
  "Under review": "Bajo revisión",
  "Published": "Publicado",
  "Page Title": "Título de la página",
  "Copy Link": "Copiar link",
  "Copy Text": "Copiar texto",
  "Copy as HTML": "Copiar como HTML",
  "Download Media": "Descargar medios",
  "Account under review": "Cuenta en revisión",
  "My Files": "Mis archivos",
  "Upload your files": "Sube tus archivos",
  "Add File": "Agregar archivo",
  "File description": "Descripción del archivo",
  "File name": "Nombre del archivo",
  "Level 1": "Nivel 1",
  "Level 2": "Nivel 2",
  "Level 3": "Nivel 3",
  "Level 4": "Nivel 4",
  "Level 5": "Nivel 5",
  "Level 6": "Nivel 6",
  "Level 7": "Nivel 7",
  "Level 8": "Nivel 8",
  "Level 9": "Nivel 9",
  "Level 10": "Nivel 10",
  "Domain": "Dominio",
  "Conversion": "Conversión",
  "Origin": "Origen",
  "Origin Domain": "Dominio de origen",
  "Source": "Fuente",
  "Landing Page": "Página de destino",
  "Custom Query Param": "Parámetros de consulta personalizados",
  "Query Parameter": "Parámetro de consulta",
  "Aggregate By": "Agregado por",
  "No Data": "Sin datos",
  "Quantity": "Cantidad",
  "Price": "Precio",
  "Customer": "Cliente",
  "Order Total": "Coste Pedido",
  "Products Sold": "Productos vendidos",
  "networkLink": "Enlaces para referir",
  "Discount Adjustment": "Ajuste de descuento",
  "My Pages": "Mis páginas",
  "Are you sure you want to delete this page ?": "Estás seguro de que quieres eliminar esta página ?",
  "Sponsor": "Patrocinador",
  "Referred by": "Referido por",
  "Gift Card": "Tarjeta de regalo",
  "Customers": "Clientes",
  "Show all": "Mostrar todo",
  "sale_approved": "aprobado",
  "sale_pending": "pendiente",
  "Traffic": "Tráfico",
  "Direct": "Directo",
  "Sort By": "Ordenar por",
  "Shipping Address": "Dirección de Envío",
  "discount_page_heading": "Aquí está su {descuento} OFF",
  "discount_page_text": "{affiliate_name} cree que te encantará {store_name}, así que aquí tienes {discount} de descuento.",
  "SHOP NOW": "COMPRA AHORA",
  "leaderboard_rank": "{amount} detrás de #{rank}",
  "Use discount code": "Usar código de descuento",
  "Minimum Amount": "Minimo: {amount}",
  "Maximum Amount": "Máximo: {amount}",
  "Spend": "Gastar",
  "Move the slider to select the amount you want to redeem": "Mueva el control deslizante para seleccionar la cantidad que desea canjear",
  "Get discount code worth": "Obtenga un código de descuento por valor",
  "Redeem": "Redimir",
  "Here is your amount discount coupon": "Aquí está su cupón de descuento {amount}",
  "Due in x days": "Vencimiento en {x} días",
  "recurring_plan_daily": "{num, plural, one {día} other {{num} días}}",
  "recurring_plan_weekly": "{num, plural, one {semana} other {{num} semanas}}",
  "recurring_plan_monthly": "{num, plural, one {mo} other {{num} mo}}",
  "recurring_plan_yearly": "año",
  "Activate membership": "Activar membresía",
  "Continue": "Seguir",
  "Choose": "Escoger",
  "Current Password": "Contraseña actual",
  "Confirm Password": "Confirmar contraseña",
  "Password was successfully changed": "La contraseña se cambió con éxito",
  "Passwords do not match": "Las contraseñas no coinciden",
  "Profile Updated": "Perfil actualizado",
  "Personal Discount": "Descuento personal",
  "Personal discount description": "Utilice el siguiente código de cupón para sus compras personales para recibir descuentos en su pedido",
  "Free shipping": "Envío gratis",
  "This month": "Este mes",
  "Last month": "El mes pasado",
  "Last 30 days": "Últimos 30 días",
  "All time": "Todo el tiempo",
  "OK": "Okay",
  "Bank Transfer (SEPA)": "Transferencia bancaria (SEPA)",
  "PayPal email address": "Email de Paypal",
  "Website": "Sitio web",
  "Add to cart": "Añadir al carrito",
  "Store": "tienda",
  "Shop": "Tienda",
  "Download invoice": "Descargar factura",
  "Transactions": "Actas",
  "Notification preferences": "Preferencias de notificación",
  "New sale notification": "Notificación de nueva venta",
  "Sale updated notification": "Notificación actualizada de venta",
  "Payment processed notification": "Notificación de pago procesado",
  "Newsletter email": "Correo electrónico del boletín",
  "Available: Inventory": "Disponible: {totalInventory, number}",
  "Complete Checkout": "Pago completo",
  "Remove": "Eliminar",
  "Open Checkout page": "Abrir página de pago",
  "Checkout": "Verificar",
  "Network explorer": "Explorador de red",
  "Number of sales": "Numero de ventas",
  "Total sale volume": "Volumen total de venta",
  "Total earnings": "Ganancias Totales",
  "Summary": "Resumen",
  "Status": "Estado",
  "Add note": "Añadir la nota",
  "Note": "Nota",
  "Search": "Buscar",
  "Show active affiliates": "Mostrar afiliados activos",
  "Transaction": "Transacción",
  "Final Balance": "Saldo final",
  "Pending": "Pendiente",
  "Approved": "Aprobado",
  "Deleted": "Eliminado",
  "Rejected": "Rechazado",
  "Network commission": "Comisión de red",
  "Wallet Adjustment": "Ajuste de billetera",
  "Signup Bonus": "Bonus al registrarse",
  "Recruitment Bonus": "Bono de contratación",
  "Target Bonus": "Bono objetivo",
  "Page currentPageNumber of totalPages": "Página {page, number} de {totalPages, number}",
  "networkCommissionDescription":""
}
