require('dotenv').config()
const googleTranslate = require('google-translate')("AIzaSyBiDy04e9m5hYKi5z4vcYDfxvYxN5Oe-XU");
const en = require('./en');
const langs = ['ro','ru','es','de','el','fr','pt','ru','sv','it','pt','nb','pl', 'iw','tr','ar','nl','ja','zh_CN','zh_TW','hu','vi','ko','fi','cs','da','lt','th','ms'];
const fs = require('fs');
const path = require('path');

const p = langs.map(async (lang) => {
	const map = require(`./${lang}.js`);

	const promises = Object.keys(en).map((item) => new Promise((resolve, reject) => {
		if (map[item]){
			return resolve({ key: item, val: map[item] });
		}
		googleTranslate.translate(en[item], lang, (err, translation) => {
			if (err){
				console.log(err);
				//reject(err);
				resolve()
			}
			else {
				console.log('translated',lang, en[item])
				resolve({ key: item, val: translation.translatedText });
			}
		});
	}));

	return Promise.all(promises).then((res) => {
		const langMap = {};
		res.forEach((item) => {
			if(item){langMap[item.key] = item.val;}
		});
		const str = `module.exports = ${JSON.stringify(langMap, null, 2)}`;
		fs.writeFileSync(path.join(__dirname,`${lang}.js`), str);
	});
});


function updatePublic(){

	[...langs,'en'].forEach((lang)=>{
		const json = require(`./${lang}.js`);
		fs.writeFileSync(path.join(__dirname,'..','..','public','locales',lang+".json"), JSON.stringify(json),'utf-8')
	});
}

module.exports = {
	updatePublic
}

Promise.all(p).then(() => process.exit(200));
