module.exports = {
  "tryAgain": "נסה שוב",
  "contactUs": "תיצור איתנו קשר",
  "navlinkHome": "בית",
  "navlinkProducts": "מוצרים",
  "navlinkPayments": "תשלומים",
  "navlinkSettings": "הגדרות",
  "logout": "להתנתק",
  "accountBannedMessage": "החשבון שלך הורחק. ההפניות שלך לא יזוכו עוד. כדי לערער על האיסור, צור איתנו קשר בכתובת partners@goaffpro.com",
  "referralLink": "קישור הפנייה",
  "referralLinkBadge": "{commission_value}",
  "referralLinkDesc": "הפנה את חבריך באמצעות הקישור שלהלן וקבל עמלות על רכישות שבוצעו על ידם",
  "referralCode": "קוד הפניה",
  "referralCodeDesc": "התאם אישית את קוד ההפניה של כתובת האתר שלך",
  "Profile": "פרופיל",
  "ProfileDesc": "עדכן את הפרופיל שלך לקבלת תוצאות טובות יותר",
  "PaymentSettings": "הגדרות תשלום",
  "PaymentSubtitle": "איך היית רוצה לקבל תשלום?",
  "paymentTerm": "טווח תשלום",
  "minimumPayout": "תשלום מינימלי",
  "Download": "הורד",
  "amountPaid": "סכום ששולם",
  "amountPending": "סכום ממתין",
  "lastPayment": "תשלום אחרון",
  "summary": "סיכום",
  "details": "פרטים",
  "noPayments": "עדיין לא בוצעו תשלומים",
  "productLinks": "קישורים למוצר",
  "productLinkSubtitle": "עיין בחנות למוצרים מתאימים. הדבק את כתובת האתר להלן כדי ליצור קישור שותפים",
  "productLinkDescription": "שתף את הקישור בבלוג שלך, Facebook, Instagram וכו 'כאשר אנשים לבקר בחנות באמצעות הקישור שלך אתה מקבל עמלות על כל מה שהם קונים",
  "productPageLinkText": "הדבק קישור לדף המוצר",
  "generatedAffiliateLinkText": "הקישור שנוצר שלך",
  "createLinkButton": "צור קישור",
  "couponCode": "קוד קופון",
  "couponCodeSubtitle": "שתף את קוד הקופון שלך עם אחרים. עבור כל רכישה מישהו עושה באמצעות קוד הקופון שלך, אתה מקבל את הקרדיט",
  "changeCoupon": "שנה קוד קופון",
  "noCouponGiven": "לא קיבלתם קופון לחלוק. תוכל לבקש מאתנו שובר שתשתף עם העוקבים שלך",
  "mediaAssets": "נכסי מדיה",
  "Cancel": "בטל",
  "Submit": "שלח",
  "Update": "עדכון",
  "Change": "שינוי",
  "Setup": "להכין",
  "SaveChanges": "שמור שינויים",
  "changeCouponModalDescription": "שנה קוד קופון בהתאם להעדפותיך. הקוד הישן שלך יבוטל",
  "createAccount": "צור חשבון",
  "Create Account": "צור חשבון",
  "emailAddress": "כתובת דוא\"ל",
  "password": "סיסמה",
  "yourName": "השם שלך",
  "Your Name": "השם שלך",
  "Login": "התחברות",
  "alreadyHaveAccount": "כבר יש לך חשבון ?",
  "dontHaveAccount": "אין לך חשבון?",
  "tosAgree": "על ידי יצירת חשבון אתה מסכים במפורש לתנאים וההגבלות ולמדיניות הפרטיות שלנו",
  "signInGoogle": "היכנס באמצעות Google",
  "signInFacebook": "התחבר עם פייסבוק",
  "forgotPassword": "שכחת את הסיסמא",
  "emailVerified": "כתובת אימייל מאושרת",
  "emailVerifiedThanks": "תודה על שאישרת את כתובת הדוא\"ל שלך.",
  "returnToDashboard": "חזור למרכז השליטה",
  "verifyingEmail": "מאמת את כתובת הדוא\"ל",
  "recoverPassword": "שחזור סיסמה",
  "recoverPasswordFormDesc": "הזן את כתובת האימייל שלך למטה. אנו נשלח לך קישור לאיפוס הסיסמה שלך בדוא\"ל",
  "recoverLinkSent": "קישור לאיפוס סיסמה נשלח לכתובת הדוא\"ל שלך {email}",
  "resetPassword": "לאפס את הסיסמה",
  "newPassword": "סיסמה חדשה",
  "pageTitle": "פורטל שותפים",
  "joinNow": "הצטרף עכשיו",
  "heroline": "רווח מהניסיון שלנו",
  "herosubtext": "",
  "heroDescHasCommission": "הרווח עד עמלה {commission_value} עבור כל הפניה מוצלחת",
  "heroDescNoCommission": "להרוויח עמלות על הפניות מוצלחות",
  "sectionHeadingHowItWorks": "איך זה עובד ?",
  "sectionCardFirstTitle": "הצטרף",
  "sectionCardFirstDetail": "זה בחינם וקל להצטרף. קום היום. {הצטרף עכשיו}",
  "sectionCardSecondTitle": "פרסם",
  "sectionCardSecondDetail": "בחר מתוך המוצרים שלנו כדי לפרסם את הלקוחות שלך. בין אם אתה רשת גדולה, אתר תוכן, משפיע על מדיה חברתית או בלוגר, יש לנו כלי קישור פשוטים כדי לענות על צורכי הפרסום שלך ולעזור לך לייצר רווחים {link_tools}",
  "linkingTools": "קישור כלים",
  "productCommissions": "עמלות מוצרים",
  "Commission Details": "פרטי הנציבות",
  "reportingTools": "כלי דיווח",
  "Reports": "דיווחים",
  "Tools": "כלים",
  "sectionCardThirdTitle": "להרוויח",
  "sectionCardThirdCommDetail": "קבל עד {commission_value} בעמלות על הפניות מוצלחות. הרווח עמלות מכל הרכישות המזכות, לא רק את המוצרים שפרסמת. בנוסף, שיעורי ההמרות התחרותיים שלנו עוזרים לך למקסם את הרווחים שלך. {link_commissions}",
  "footerLearnMore": "למד",
  "footerCustomerSupport": "שירות לקוחות",
  "footerLegal": "משפטי",
  "footerCommissions": "עמלות",
  "footerTools": "כלים",
  "footerReporting": "דיווח",
  "footerTerms": "תנאים והתניות",
  "privacyPolicy": "מדיניות פרטיות",
  "footerFeedbackTitle": "מה אתה חושב?",
  "footerFeedbackDescription": "האם יש לך הצעה או הערה על התוכנית שלנו?",
  "footerFeedbackLetUsKnow": "תודיע לנו.",
  "pageDesc": "שלח לנו את השאילתות, הצעות, בעיות וכו 'באמצעות הטופס שלהלן. אנו נחזור אליך בהקדם האפשרי",
  "issueTypeLabel": "בחר סוג גיליון",
  "issueTypeGeneral": "בעיות כלליות / שאילתות",
  "issueTypeTechnical": "בעיות טכניות / שאילתות",
  "issueTypeFeedback": "משוב / הצעות",
  "issueTypePayment": "בעיות תשלום / שאילתות",
  "issueTypeDelAcct": "בקשת מחיקת חשבון",
  "yourMessage": "ההודעה שלך",
  "sendMessage": "לשלוח הודעה",
  "messageSentSuccess": "תודה שפנית אלינו. קיבלנו את ההודעה שלך ואנו נחזור אליכם בהקדם",
  "emailUnverified": "שלחנו לך דוא\"ל לאימות חשבון. בטובך לבדוק את תיבת הדואר הנכנס שלך. לא קיבלת את הדואר?",
  "resendEmail": "לשלוח דוא\"ל לאימות",
  "emailSent": "הדוא\"ל נשלח",
  "accountPendingVerification": "החשבון שלך עדיין נמצא באימות. המתן 24-48 כדי לאמת את החשבון. חביב {updateYourProfile} בינתיים",
  "updateYourProfile": "עדכן את הפרופיל שלך",
  "Referrals": "הפניות",
  "Orders": "הזמנות",
  "Conversions": "המרות",
  "Sales": "מכירות",
  "Earnings": "רווחים",
  "Team sales": "מכירות קבוצתיות",
  "thisMonth": "החודש",
  "lastMonth": "חודש שעבר",
  "last30Days": "30 הימים האחרונים",
  "allTime": "כל הזמן",
  "REGISTRATIONNOTALLOWED": "מצטער! בשלב זה איננו מקבלים רישויים חדשים",
  "EMAILREQUIRED": "דרושה כתובת דוא\"ל",
  "USEREXISTS": "כבר יש לך חשבון אצלנו.",
  "clickHereToLogin": "לחץ כאן כדי להתחבר",
  "EMAILINVALID": "בדוק את כתובת הדוא\"ל שלך. זה נראה לא נכון",
  "INCORRECTPASSWORD": "כתובת האימייל או הסיסמה אינן נכונות.",
  "clickHereToResetPwd": "לחץ כאן כדי לאפס את הסיסמה שלך",
  "createAnAccount": "צור חשבון",
  "LINKEXPIRED": "פג תוקפו של הקישור לאיפוס הסיסמה. כדי ליצור קישור חדש, השתמש באפשרות שכחת סיסמה.",
  "NETWORKERROR": "תקלת רשת.",
  "USERDOESNOTEXISTS": "אין לך חשבון אצלנו.",
  "Create Password": "צור סיסמה",
  "Show password": "הראה סיסמה",
  "Network": "רשת",
  "Network Summary": "סיכום רשת",
  "Network Commissions": "עמלות רשת",
  "Partner signup referral link": "קישור להפניה של שותף",
  "Get commissions when your sub affiliates refer customers": "קבל עמלות כאשר שותפי המשנה שלך מתייחסים ללקוחות",
  "Commissions": "עמלות",
  "My Network": "הרשת שלי",
  "No direct referrals yet": "אין הפניות ישירות עדיין",
  "Commission": "עמלה",
  "Level": "רמה",
  "Direct Referrals": "הפניות ישירות",
  "Indirect Referrals": "הפניות עקיפות",
  "Bank Transfer": "העברה בנקאית",
  "Store Discount Coupon": "חנות קופון הנחה",
  "Bank Name": "שם הבנק",
  "Account Type": "סוג החשבון",
  "Checking": "בודק",
  "Saving": "שומר",
  "Account Name": "שם החשבון",
  "Account Number": "מספר חשבון",
  "Branch Code": "קוד סניף",
  "Swift Code": "קוד זיהוי בנק",
  "Phone": "מכשיר טלפון",
  "Comment": "תגובה",
  "Country": "מדינה",
  "Hide Links": "הסתר קישורים",
  "Show Links": "הצג קישורים",
  "Invite others, grow your network, and earn commissions": "הזמן אחרים, הרחב את הרשת שלך וקבל עמלות",
  "Signup Link": "קישור הרשמה",
  "Home Page": "דף הבית",
  "Details": "פרטים",
  "Members": "חברים",
  "Revenue": "הכנסות",
  "Commisssion": "עמלה",
  "Copy Shareable Link": "העתק קישור הניתן לשיתוף",
  "Copy": "עותק",
  "Copied": "הועתק",
  "Net 7": "נטו 7",
  "Net 15": "נטו 15",
  "Net 30": "נטו 30",
  "OR": "או",
  "Go Back": "תחזור",
  "Debit Card": "כרטיס חיוב",
  "Debit Card Number": "מספר כרטיס חיוב",
  "First Name": "שם פרטי",
  "Last Name": "שם משפחה",
  "Name": "שם",
  "Document Number": "מספר מסמך",
  "required": "* - נדרש",
  "Password": "סיסמה",
  "Marketing Tools": "כלי שיווק",
  "coupon_code_discount": "{discount_value} כבוי",
  "Payment Mode": "מצב תשלום",
  "No payment mode set": "לא הוגדר מצב תשלום",
  "Primary": "ראשי",
  "Referral codes (one per line)": "קודי הפניה (אחד בכל שורה)",
  "First one will be set as primary": "הראשון ייקבע כראשוני",
  "Change Password": "שנה סיסמא",
  "Date": "תאריך",
  "Order": "להזמין",
  "Amount": "כמות",
  "Top Selling Products": "למעלה מכירת מוצרים",
  "Network Map": "מפת רשת",
  "Address": "כתובת",
  "Zip Code": "מיקוד",
  "City": "עיר",
  "State": "מדינה",
  "Postal Code": "מיקוד",
  "Street Address": "כתובת רחוב",
  "Tax Identification Number": "מספר זיהוי מס",
  "EIN or SSN or PAN (whichever is applicable)": "EIN או SSN או PAN (לפי העניין)",
  "Your mailing address": "כתובת הדואר שלך",
  "I agree to terms and conditions": "אני מסכים לתנאים וההגבלות",
  "Your rank": "הדרגה שלך",
  "You": "אתה",
  "Analytics": "אנליטיקס",
  "Loading Data...It might take a while": "טוען נתונים ... זה עלול לקחת זמן",
  "Browsers": "דפדפנים",
  "Operating Systems": "מערכות הפעלה",
  "Devices": "מכשירים",
  "Click Origins": "לחץ על מקורות",
  "Landing Pages": "דפי נחיתה",
  "Visits": "ביקורים",
  "Traffic Analysis": "ניתוח תנועה",
  "This Week": "השבוע",
  "We are not accepting registrations at this moment. Kindly check back later": "איננו מקבלים הרשמות ברגע זה. אנא חזרו בהמשך",
  "Cheque": "בדוק",
  "Commission Structure": "מבנה הנציבות",
  "Product": "מוצר",
  "Category": "קטגוריה",
  "A {commission_value} commission is given on sale of every product (except those listed below)": "עמלה {provision_value} ניתנת למכירה של כל מוצר (למעט אלה המפורטים להלן)",
  "Pages": "דפים",
  "You have not published any pages yet. {create_page_link}": "עדיין לא פרסמת דפים. {create_page_link}",
  "Create a new page": "צור דף חדש",
  "New Page": "עמוד חדש",
  "Edit Page": "ערוך עמוד",
  "Save Page": "שמור דף",
  "Preview Page": "תצוגה מקדימה של דף",
  "Under review": "בבדיקה",
  "Published": "יצא לאור",
  "Page Title": "כותרת העמוד",
  "Copy Link": "העתק קישור",
  "Copy Text": "העתק טקסט",
  "Copy as HTML": "העתק כ- HTML",
  "Download Media": "הורד מדיה",
  "Account under review": "חשבון שנבדק",
  "My Files": "הקבצים שלי",
  "Upload your files": "העלה את הקבצים שלך",
  "Add File": "הוסף קובץ",
  "File description": "תיאור הקובץ",
  "File name": "שם קובץ",
  "Level 1": "שלב 1",
  "Level 2": "שלב 2",
  "Level 3": "רמה 3",
  "Level 4": "דרגה 4",
  "Level 5": "דרגה 5",
  "Level 6": "דרגה 6",
  "Level 7": "דרגה 7",
  "Level 8": "דרגה 8",
  "Level 9": "דרגה 9",
  "Level 10": "דרגה 10",
  "Domain": "תחום",
  "Conversion": "המרה",
  "Origin": "מוצא",
  "Origin Domain": "תחום מוצא",
  "Source": "מקור",
  "Landing Page": "דף נחיתה",
  "Custom Query Param": "פרמטר שאילתה מותאם אישית",
  "Query Parameter": "פרמטר שאילתה",
  "Aggregate By": "מצטבר מאת",
  "No Data": "אין מידע",
  "Quantity": "כמות",
  "Price": "מחיר",
  "Customer": "צרכן",
  "Order Total": "סך כל ההזמנה",
  "Products Sold": "מוצרים שנמכרו",
  "networkLink": "קישור רשת",
  "Discount Adjustment": "התאמת הנחה",
  "My Pages": "הדפים שלי",
  "Are you sure you want to delete this page ?": "האם אתה בטוח שברצונך למחוק דף זה?",
  "Sponsor": "נותן חסות",
  "Referred by": "הופנה על ידי",
  "Gift Card": "כרטיס מתנה",
  "Customers": "לקוחות",
  "Show all": "הצג הכול",
  "sale_approved": "אושר",
  "sale_pending": "ממתין ל",
  "Traffic": "תנועה",
  "Direct": "ישיר",
  "Sort By": "מיין לפי",
  "Shipping Address": "כתובת למשלוח",
  "discount_page_heading": "הנה {הנחה} הנחה שלך",
  "discount_page_text": "{affiliate_name} חושב שתאהב את {store_name}, אז הנה {הנחה} כבוי!",
  "SHOP NOW": "קנה עכשיו",
  "leaderboard_rank": "{סכום} מאחורי # {דרגה}",
  "Use discount code": "השתמש בקוד הנחה",
  "Minimum Amount": "כמות מינימלית}",
  "Maximum Amount": "כמות מקסימלית}",
  "Spend": "לְבַלוֹת",
  "Move the slider to select the amount you want to redeem": "הזז את המחוון כדי לבחור את הסכום שברצונך לממש",
  "Get discount code worth": "קבל שווי הנחה בשווי",
  "Redeem": "לִפְדוֹת",
  "Here is your amount discount coupon": "להלן קופון ההנחה של {amount}",
  "Due in x days": "פירעון בעוד {x} ימים",
  "recurring_plan_daily": "{מספר, רבים, אחד {יום} אחר {{מספר} ימים}}",
  "recurring_plan_weekly": "{מספר, רבים, {שבוע} אחר {{מספר} שבועות}}",
  "recurring_plan_monthly": "{מספר, רבים, אחד {מו} אחר {{מספר} מו}}",
  "recurring_plan_yearly": "שָׁנָה",
  "Activate membership": "הפעל חברות",
  "Continue": "לְהַמשִׁיך",
  "Choose": "בחר",
  "Current Password": "סיסמה נוכחית",
  "Confirm Password": "אשר סיסמה",
  "Password was successfully changed": "הסיסמה שונתה בהצלחה",
  "Passwords do not match": "סיסמאות לא תואמות",
  "Profile Updated": "הפרופיל עודכן",
  "Personal Discount": "הנחה אישית",
  "Personal discount description": "השתמש בקוד הקופון הבא לרכישות האישיות שלך כדי לקבל הנחות בהזמנתך",
  "Free shipping": "משלוח חינם",
  "This month": "החודש",
  "Last month": "חודש שעבר",
  "Last 30 days": "30 הימים האחרונים",
  "All time": "כל הזמן",
  "OK": "בסדר",
  "Bank Transfer (SEPA)": "העברה בנקאית (SEPA)",
  "PayPal email address": "כתובת דוא\"ל של Paypal",
  "Website": "אתר אינטרנט",
  "Add to cart": "הוסף לעגלה",
  "Store": "לִקְנוֹת",
  "Shop": "לִקְנוֹת",
  "Download invoice": "הורד חשבונית",
  "Transactions": "עסקאות",
  "Notification preferences": "העדפות הודעה",
  "New sale notification": "הודעת מכירה חדשה",
  "Sale updated notification": "הודעה על מכירה מעודכנת",
  "Payment processed notification": "הודעה על עיבוד התשלום",
  "Newsletter email": "דוא\"ל עלון",
  "Available: Inventory": "זמין: {totalInventory, number}",
  "Complete Checkout": "קופה מלאה",
  "Remove": "לְהַסִיר",
  "Open Checkout page": "פתח את דף הקופה",
  "Checkout": "לבדוק",
  "Network explorer": "חוקר רשת",
  "Number of sales": "מספר המכירות",
  "Total sale volume": "נפח מכירה כולל",
  "Total earnings": "סך ההכנסות",
  "Summary": "סיכום",
  "Status": "סטָטוּס",
  "Add note": "להוסיף הערה",
  "Note": "הערה",
  "Search": "לחפש",
  "Show active affiliates": "הצג שותפים פעילים",
  "Transaction": "עִסקָה",
  "Final Balance": "איזון סופי",
  "Pending": "ממתין ל",
  "Approved": "אושר",
  "Deleted": "נמחק",
  "Rejected": "נִדחֶה",
  "Network commission": "ועדת רשת",
  "Wallet Adjustment": "התאמת ארנק",
  "Signup Bonus": "בונוס הרשמה",
  "Recruitment Bonus": "בונוס גיוס",
  "Target Bonus": "בונוס יעד",
  "Page currentPageNumber of totalPages": "דף {page, number} מתוך {totalPages, number}",
  "networkCommissionDescription":""

}
