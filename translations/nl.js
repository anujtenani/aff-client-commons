module.exports = {
  "tryAgain": "Probeer opnieuw",
  "contactUs": "Neem contact op",
  "navlinkHome": "Huis",
  "navlinkProducts": "Producten",
  "navlinkPayments": "Betalingen",
  "navlinkSettings": "Instellingen",
  "logout": "Uitloggen",
  "accountBannedMessage": "Uw account is geblokkeerd! U ontvangt niet langer krediet voor uw verwijzingen.",
  "referralLink": "Referentie link",
  "referralLinkBadge": "{commission_value}",
  "referralLinkDesc": "Verwijs uw vrienden door via de onderstaande link en verdien commissies op aankopen die zij doen",
  "referralCode": "Verwijzingscode",
  "referralCodeDesc": "Pas uw URL-verwijzingscode aan",
  "Profile": "Profiel",
  "ProfileDesc": "Update je profiel",
  "PaymentSettings": "Betalingsinstellingen",
  "PaymentSubtitle": "Hoe wilt u betaald worden?",
  "paymentTerm": "Betalingstermijn",
  "minimumPayout": "Minimum uitbetaling",
  "Download": "Download",
  "amountPaid": "Betaald bedrag",
  "amountPending": "Bedrag in behandeling",
  "lastPayment": "Laatste betaling",
  "summary": "Samenvatting",
  "details": "Details",
  "noPayments": "Er zijn nog geen betalingen gedaan",
  "productLinks": "Product links",
  "productLinkSubtitle": "Blader door de winkel voor in aanmerking komende producten. Plak de onderstaande URL om een affiliatielink te genereren",
  "productLinkDescription": "Deel deze link op uw blog, Facebook, Instagram, enz. Wanneer mensen de winkel bezoeken met uw link, krijgt u commissies op alles wat ze kopen",
  "productPageLinkText": "Link naar productpagina plakken",
  "generatedAffiliateLinkText": "Uw gegenereerde link",
  "createLinkButton": "Maak een link",
  "couponCode": "Coupon Code",
  "couponCodeSubtitle": "Deel uw couponcode met anderen. Voor elke aankoop die iemand doet met behulp van uw couponcode, krijgt u het tegoed",
  "changeCoupon": "Wijzig couponcode",
  "noCouponGiven": "Geen couponcode gevonden! Je kunt ons om een couponcode vragen die we met je volgers kunnen delen.",
  "mediaAssets": "Media-middelen",
  "Cancel": "Annuleren",
  "Submit": "Voorleggen",
  "Update": "Bijwerken",
  "Change": "Verandering",
  "Setup": "Opstelling",
  "SaveChanges": "Wijzigingen opslaan",
  "changeCouponModalDescription": "Wijzig de couponcode volgens uw voorkeur. Uw oude code wordt ongeldig gemaakt",
  "createAccount": "Account aanmaken",
  "Create Account": "Account aanmaken",
  "emailAddress": "E-mailadres",
  "password": "Wachtwoord",
  "yourName": "Uw naam",
  "Your Name": "Uw naam",
  "Login": "Log in",
  "alreadyHaveAccount": "Heb je al een account?",
  "dontHaveAccount": "Heb je nog geen account?",
  "tosAgree": "Door een account aan te maken, gaat u uitdrukkelijk akkoord met onze Servicevoorwaarden & Privacybeleid",
  "signInGoogle": "Google",
  "signInFacebook": "Facebook",
  "forgotPassword": "Wachtwoord vergeten",
  "emailVerified": "Email-adres geverifieërd",
  "emailVerifiedThanks": "Bedankt voor het bevestigen van uw e-mailadres.",
  "returnToDashboard": "Keer terug naar Dashboard",
  "verifyingEmail": "Verifiëren van e-mailadres",
  "recoverPassword": "Herstel wachtwoord",
  "recoverPasswordFormDesc": "Vul hieronder uw e-mailadres in. We sturen u een link om uw wachtwoord opnieuw in te stellen via uw e-mail",
  "recoverLinkSent": "Er is een link voor het opnieuw instellen van het wachtwoord verzonden naar uw e-mailadres {email}",
  "resetPassword": "Reset wachtwoord",
  "newPassword": "Nieuw paswoord",
  "pageTitle": "Affiliate Portal",
  "joinNow": "Doe nu mee",
  "heroline": "Profiteer van onze ervaring",
  "herosubtext": "",
  "heroDescHasCommission": "Verdien tot {commission_value} commissie voor elke succesvolle verwijzing",
  "heroDescNoCommission": "Verdien commissies bij succesvolle verwijzingen",
  "sectionHeadingHowItWorks": "Hoe werkt het ?",
  "sectionCardFirstTitle": "Toetreden",
  "sectionCardFirstDetail": "Het is gratis en gemakkelijk om lid te worden. Ga vandaag aan de slag. {join_now}",
  "sectionCardSecondTitle": "Reclame maken",
  "sectionCardSecondDetail": "Kies uit onze producten om te adverteren bij uw klanten. Of u nu een groot netwerk, inhoudssite, social media-beïnvloeder of blogger bent, we hebben eenvoudige koppelingstools om aan uw advertentiebehoeften te voldoen en u te helpen inkomsten te genereren met",
  "linkingTools": "Hulpmiddelen voor koppelen",
  "productCommissions": "Productcommissies",
  "Commission Details": "Commissie details",
  "reportingTools": "Rapportagetools",
  "Reports": "rapporten",
  "Tools": "Hulpmiddelen",
  "sectionCardThirdTitle": "Verdienen",
  "sectionCardThirdCommDetail": "Krijg tot {commission_value} in commissies voor succesvolle verwijzingen. Verdien commissies uit alle in aanmerking komende aankopen, niet alleen de producten waarvoor u adverteerde. Bovendien helpen onze concurrerende conversiepercentages u om uw inkomsten te maximaliseren.",
  "footerLearnMore": "Leren",
  "footerCustomerSupport": "Klantenservice",
  "footerLegal": "wettelijk",
  "footerCommissions": "commissies",
  "footerTools": "Hulpmiddelen",
  "footerReporting": "Rapportage",
  "footerTerms": "algemene voorwaarden",
  "privacyPolicy": "Privacybeleid",
  "footerFeedbackTitle": "Wat denk je?",
  "footerFeedbackDescription": "Heeft u een suggestie of opmerking over ons programma?",
  "footerFeedbackLetUsKnow": "Laat het ons weten.",
  "pageDesc": "Stuur ons uw vragen, suggesties, problemen etc. met behulp van het onderstaande formulier. We nemen zo snel mogelijk contact met u op",
  "issueTypeLabel": "Selecteer het probleemtype",
  "issueTypeGeneral": "Algemene problemen / vragen",
  "issueTypeTechnical": "Technische problemen / vragen",
  "issueTypeFeedback": "Feedback / suggesties",
  "issueTypePayment": "Betalingsproblemen / vragen",
  "issueTypeDelAcct": "Verzoek om verwijdering van account",
  "yourMessage": "Uw bericht",
  "sendMessage": "Bericht versturen",
  "messageSentSuccess": "Bedankt dat je contact met ons hebt opgenomen. We hebben uw bericht ontvangen en nemen zo spoedig mogelijk contact met u op",
  "emailUnverified": "We hebben u een e-mail voor accountverificatie gestuurd. Controleer alstublieft uw inbox. Heb je de mail niet gekregen?",
  "resendEmail": "Verificatie-e-mail opnieuw verzenden",
  "emailSent": "Email verzonden",
  "accountPendingVerification": "Uw account is nog onder verificatie. Wacht tot 24-48 voordat het account wordt geverifieerd. In de tussentijd vriendelijk {updateYourProfile}",
  "updateYourProfile": "update je profiel",
  "Referrals": "Verwijzingen",
  "Orders": "bestellingen",
  "Conversions": "conversies",
  "Sales": "verkoop",
  "Earnings": "verdiensten",
  "Team sales": "Team verkoop",
  "thisMonth": "Deze maand",
  "lastMonth": "Vorige maand",
  "last30Days": "Laatste 30 dagen",
  "allTime": "Altijd",
  "REGISTRATIONNOTALLOWED": "Sorry! We accepteren momenteel geen nieuwe registraties",
  "EMAILREQUIRED": "E-mailadres is verplicht",
  "USEREXISTS": "U heeft al een account bij ons.",
  "clickHereToLogin": "Klik hier om in te loggen",
  "EMAILINVALID": "Controleer uw e-mailadres. Het lijkt onjuist",
  "INCORRECTPASSWORD": "E-mailadres of wachtwoord is onjuist.",
  "clickHereToResetPwd": "Klik hier om uw wachtwoord opnieuw in te stellen",
  "createAnAccount": "Account aanmaken",
  "LINKEXPIRED": "De link voor het opnieuw instellen van het wachtwoord is verlopen. Gebruik de optie Wachtwoord vergeten om een nieuwe link te genereren.",
  "NETWORKERROR": "Netwerkfout.",
  "USERDOESNOTEXISTS": "U heeft geen account bij ons.",
  "Create Password": "Maak een wachtwoord",
  "Show password": "Laat wachtwoord zien",
  "Network": "Netwerk",
  "Network Summary": "Netwerk samenvatting",
  "Network Commissions": "Netwerkcommissies",
  "Partner signup referral link": "Doorverwijslink voor partneraanmelding",
  "Get commissions when your sub affiliates refer customers": "Ontvang commissies wanneer uw subpartners klanten verwijzen",
  "Commissions": "commissies",
  "My Network": "Mijn netwerk",
  "No direct referrals yet": "Nog geen directe verwijzingen",
  "Commission": "Commissie",
  "Level": "Niveau",
  "Direct Referrals": "Directe verwijzingen",
  "Indirect Referrals": "Indirecte verwijzingen",
  "Bank Transfer": "Bankoverschrijving",
  "Store Discount Coupon": "Kortingsbon winkel",
  "Bank Name": "Banknaam",
  "Account Type": "account type",
  "Checking": "controleren",
  "Saving": "Besparing",
  "Account Name": "Accountnaam",
  "Account Number": "Rekeningnummer",
  "Branch Code": "Filiaalcode",
  "Swift Code": "Swift code",
  "Phone": "Telefoon",
  "Comment": "Commentaar",
  "Country": "land",
  "Hide Links": "Links verbergen",
  "Show Links": "Links weergeven",
  "Invite others, grow your network, and earn commissions": "Nodig anderen uit, laat uw netwerk groeien en verdien commissies",
  "Signup Link": "Link aanmelden",
  "Home Page": "Startpagina",
  "Details": "Details",
  "Members": "leden",
  "Revenue": "Omzet",
  "Commisssion": "Commissie",
  "Copy Shareable Link": "Kopieerbare link",
  "Copy": "Kopiëren",
  "Copied": "gekopieerde",
  "Net 7": "Net 7",
  "Net 15": "Netto 15",
  "Net 30": "Netto 30",
  "OR": "OF",
  "Go Back": "Ga terug",
  "Debit Card": "Debetkaart",
  "Debit Card Number": "Debit Card nummer",
  "First Name": "Voornaam",
  "Last Name": "Achternaam",
  "Name": "Naam",
  "Document Number": "Document Nummer",
  "required": "* - verplicht",
  "Password": "Wachtwoord",
  "Marketing Tools": "Marketingtools",
  "coupon_code_discount": "{discount_value} uit",
  "Payment Mode": "Betaalmethode",
  "No payment mode set": "Geen betalingsmodus ingesteld",
  "Primary": "primair",
  "Referral codes (one per line)": "Verwijzingscodes (één per regel)",
  "First one will be set as primary": "De eerste wordt ingesteld als primair",
  "Change Password": "Wachtwoord wijzigen",
  "Date": "Datum",
  "Order": "Bestellen",
  "Amount": "Bedrag",
  "Top Selling Products": "Best verkopende producten",
  "Network Map": "Netwerkkaart",
  "Address": "Adres",
  "Zip Code": "Postcode",
  "City": "stad",
  "State": "Staat",
  "Postal Code": "Postcode",
  "Street Address": "Woonadres",
  "Tax Identification Number": "Identificatienummer van de belasting",
  "EIN or SSN or PAN (whichever is applicable)": "EIN of SSN of PAN (wat van toepassing is)",
  "Your mailing address": "Uw postadres",
  "I agree to terms and conditions": "Ik ga akkoord met de algemene voorwaarden",
  "Your rank": "Jouw waardering",
  "You": "U",
  "Analytics": "Analytics",
  "Loading Data...It might take a while": "Gegevens laden ... Het kan even duren",
  "Browsers": "Browsers",
  "Operating Systems": "Besturingssystemen",
  "Devices": "Apparaten",
  "Click Origins": "Klik op Oorsprong",
  "Landing Pages": "Bestemmingspagina's",
  "Visits": "Bezoeken",
  "Traffic Analysis": "Verkeersanalyse",
  "This Week": "Deze week",
  "We are not accepting registrations at this moment. Kindly check back later": "We accepteren momenteel geen registraties. Kom later nog eens terug",
  "Cheque": "controleren",
  "Commission Structure": "Commissie structuur",
  "Product": "Product",
  "Category": "Categorie",
  "A {commission_value} commission is given on sale of every product (except those listed below)": "Een {commission_value} commissie wordt gegeven bij verkoop van elk product (behalve die hieronder vermeld)",
  "Pages": "Pages",
  "You have not published any pages yet. {create_page_link}": "U heeft nog geen pagina's gepubliceerd. {} Create_page_link",
  "Create a new page": "Maak een nieuwe pagina",
  "New Page": "Nieuwe pagina",
  "Edit Page": "Pagina aanpassen",
  "Save Page": "Sla pagina op",
  "Preview Page": "Voorbeeldpagina",
  "Under review": "Wordt beoordeeld",
  "Published": "Gepubliceerd",
  "Page Title": "Pagina titel",
  "Copy Link": "Kopieer link",
  "Copy Text": "Kopieer tekst",
  "Copy as HTML": "Kopiëren als HTML",
  "Download Media": "Media downloaden",
  "Account under review": "Account wordt beoordeeld",
  "My Files": "Mijn bestanden",
  "Upload your files": "Upload uw bestanden",
  "Add File": "Bestand toevoegen",
  "File description": "Bestandsomschrijving",
  "File name": "Bestandsnaam",
  "Level 1": "Niveau 1",
  "Level 2": "Level 2",
  "Level 3": "Niveau 3",
  "Level 4": "Niveau 4",
  "Level 5": "Niveau 5",
  "Level 6": "Niveau 6",
  "Level 7": "Niveau 7",
  "Level 8": "Niveau 8",
  "Level 9": "Niveau 9",
  "Level 10": "Niveau 10",
  "Domain": "Domein",
  "Conversion": "conversie",
  "Origin": "Oorsprong",
  "Origin Domain": "Herkomst domein",
  "Source": "Bron",
  "Landing Page": "Bestemmingspagina",
  "Custom Query Param": "Aangepaste zoekopdracht Param",
  "Query Parameter": "Zoekopdrachtparameter",
  "Aggregate By": "Samenvoegen door",
  "No Data": "Geen informatie",
  "Quantity": "Aantal stuks",
  "Price": "Prijs",
  "Customer": "Klant",
  "Order Total": "totale bestelling",
  "Products Sold": "Producten verkocht",
  "networkLink": "Netwerklink",
  "Discount Adjustment": "Kortingsaanpassing",
  "My Pages": "Mijn pagina's",
  "Are you sure you want to delete this page ?": "Weet u zeker dat u deze pagina wilt verwijderen?",
  "Sponsor": "Sponsor",
  "Referred by": "Doorverwezen door",
  "Gift Card": "Cadeaukaart",
  "Customers": "Klanten",
  "Show all": "Toon alles",
  "sale_approved": "goedgekeurd",
  "sale_pending": "in afwachting",
  "Traffic": "Verkeer",
  "Direct": "Direct",
  "Sort By": "Sorteer op",
  "Shipping Address": "afleveradres",
  "discount_page_heading": "Hier is je {discount} UIT",
  "discount_page_text": "{affiliate_name} denkt dat je {store_name} geweldig zult vinden, dus hier is {discount} korting!",
  "SHOP NOW": "WINKEL NU",
  "leaderboard_rank": "{amount} achter #{rank}",
  "Use discount code": "Gebruik kortingscode",
  "Minimum Amount": "Minimale hoeveelheid}",
  "Maximum Amount": "Maximaal aantal}",
  "Spend": "Besteden",
  "Move the slider to select the amount you want to redeem": "Verplaats de schuifregelaar om het bedrag te selecteren dat u wilt inwisselen",
  "Get discount code worth": "Krijg kortingscode waard",
  "Redeem": "Inwisselen",
  "Here is your amount discount coupon": "Hier is uw kortingsbon van {amount}",
  "Due in x days": "Vervalt over {x} dagen",
  "recurring_plan_daily": "{num, meervoud, een {dag} andere {{num} dagen}}",
  "recurring_plan_weekly": "{num, meervoud, een {week} andere {{num} weken}}",
  "recurring_plan_monthly": "{num, meervoud, een {mo} andere {{num} mo}}",
  "recurring_plan_yearly": "jaar",
  "Activate membership": "Activeer lidmaatschap",
  "Continue": "Doorgaan met",
  "Choose": "Kiezen",
  "Current Password": "Huidig wachtwoord",
  "Confirm Password": "Bevestig wachtwoord",
  "Password was successfully changed": "Wachtwoord is succesvol gewijzigd",
  "Passwords do not match": "Wachtwoorden komen niet overeen",
  "Profile Updated": "Profiel geüpdatet",
  "Personal Discount": "Persoonlijke korting",
  "Personal discount description": "Gebruik de volgende couponcode voor uw persoonlijke aankopen om kortingen op uw bestelling te ontvangen",
  "Free shipping": "Gratis verzending",
  "This month": "Deze maand",
  "Last month": "Vorige maand",
  "Last 30 days": "Laatste 30 dagen",
  "All time": "Altijd",
  "OK": "OK",
  "Bank Transfer (SEPA)": "Bankoverschrijving (SEPA)",
  "PayPal email address": "Paypal e-mailadres",
  "Website": "Website",
  "Add to cart": "Voeg toe aan winkelmandje",
  "Store": "Winkel",
  "Shop": "Winkel",
  "Download invoice": "Download factuur",
  "Transactions": "Transacties",
  "Notification preferences": "Notificatie voorkeuren",
  "New sale notification": "Melding nieuwe verkoop",
  "Sale updated notification": "Verkoop bijgewerkte melding",
  "Payment processed notification": "Melding verwerkte betaling",
  "Newsletter email": "Nieuwsbrief e-mail",
  "Available: Inventory": "Beschikbaar: {totalInventory, number}",
  "Complete Checkout": "Afrekenen voltooien",
  "Remove": "Verwijderen",
  "Open Checkout page": "Afrekenen pagina openen",
  "Checkout": "Uitchecken",
  "Network explorer": "Netwerkverkenner",
  "Number of sales": "Aantal verkopen",
  "Total sale volume": "Totaal verkoopvolume",
  "Total earnings": "Totale winst",
  "Summary": "Overzicht",
  "Status": "Toestand",
  "Add note": "Notitie toevoegen",
  "Note": "Opmerking",
  "Search": "Zoeken",
  "Show active affiliates": "Toon actieve partners",
  "Transaction": "Transactie",
  "Final Balance": "Eindsaldo",
  "Pending": "In afwachting",
  "Approved": "Goedgekeurd",
  "Deleted": "verwijderd",
  "Rejected": "Afgewezen",
  "Network commission": "netwerk commissie",
  "Wallet Adjustment": "Portemonnee aanpassing",
  "Signup Bonus": "Aanmeldingsbonus",
  "Recruitment Bonus": "Wervingsbonus",
  "Target Bonus": "Doelbonus",
  "Page currentPageNumber of totalPages": "Pagina {page, number} van {totalPages, number}",
  "networkCommissionDescription":""

}
